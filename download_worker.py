from threading import Thread
from queue import Queue, Empty
from time import sleep
import bot

queue = Queue()
workers = dict()
stop = False


def start_workers(tokens):
    global queue, workers
    for token in tokens:
        worker = DownloadWorker(token, queue)
        print('Init worker', worker.name, flush=True)
        workers[token] = worker
        worker.start()


def validate_workers():
    global queue, workers
    for token in workers.keys():
        worker = workers[token]
        if worker.is_alive() is False:
            print('Worker name: ', worker.name, ' die, restart', flush=True)
            workers[token] = DownloadWorker(token, queue)


def _control_thread():
    global stop
    while not stop:
        validate_workers()
        sleep(2)


def control_thread():
    thread = Thread(target=_control_thread, args=())
    thread.setDaemon(True)
    thread.start()


class DownloadWorker(Thread):
    def __init__(self, token: str, input_queue: Queue):
        """
        :param queue: input queue
        """
        super(DownloadWorker, self).__init__()
        self.setName(token)
        self.queue = input_queue
        self.token = token
        self.daemon = True
        self.bot = bot.Bot(self.token)
        self.stop = False

    def run(self):
        while not self.stop:
            try:
                task = self.queue.get()
                self.run_task(task)
            except Exception as e:
                print(e, flush=True)
                sleep(0.2)

    def run_task(self, task):
        try:
            self.bot.saveImage(url=task['url'], type=task['type'], user_id=task['user_id'])
        except Exception as e:
            print('run_task error{0}'.format(e), flush=True)
