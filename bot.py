import vk
import render
import urllib.request
import requests
import os
import config
import ImageWorker
import download_worker
import time
from queue import Empty


class Bot(object):
    def __init__(self, token):
        super(Bot, self).__init__()
        self.session = vk.Session(token)
        self.api = vk.API(self.session)
        self.admin_id = config.admin_id
        self.helpGroup_api = vk.API(vk.Session(config.help_group_token))
        user_session = vk.Session(config.admin_token)
        self.user_api = vk.API(user_session)

        self.anonim_session = vk.Session()
        self.anonim_api = vk.API(self.anonim_session)

        self.ready_message = """Хочешь арт? Присылай фото. Для участников проекта - вне очереди"""

    def saveImage(self, url, type, user_id):
        name = url.split('/')[-1]
        urllib.request.urlretrieve(url, 'images/' + name)
        attachment = self.createAttachment(name, type)
        self.addToImageQueue(user_id, self.ready_message, attachment)


    def createAttachment(self, image, type):
        savedImage = image
        if type == "light":
            newImage = render.render_norm(savedImage)
        elif type == "blue":
            newImage = render.render_blue(savedImage)
        elif type == "monochrome":
            newImage = render.render_monochrome(savedImage)
        photo = open(newImage, 'rb')
        files = {'file': photo}
        media_id = self.getDocMediaId(files)
        attachment = 'doc' + str(-config.help_group_id) + '_' + str(media_id)
        photo.close()
        os.remove(newImage)
        os.remove('images/' + savedImage)
        return attachment


    def getDocMediaId(self, files):
        upload_url = self.helpGroup_api.docs.getWallUploadServer(group_id=config.help_group_id)['upload_url']
        response = requests.post(upload_url, files=files).json()
        value = self.helpGroup_api.docs.save(file=response["file"])[0]
        return value['did']

    def addToImageQueue(self, user_id, message, attachment):
        ImageWorker.queue.put({'user_id': user_id, 'message': message, 'attachment': attachment})

    def addToDownloadQueue(self, url, type, user_id):
        download_worker.queue.put({'url': url, 'type': type, 'user_id': user_id})

    def checkIfAnwsered(self, user_id):
        res = False
        dialogs = self.api.messages.getHistory(user_id=97970345)
        for i in range(1, len(dialogs)):
            if 'attachments' in dialogs[i].keys():
                if dialogs[i]['attachments'][0]['type'] == 'doc':
                    res = True
        return res


    def botLogic(self):
        count = 0
        while count <= self.api.messages.getDialogs()[0]:
            dialogs = self.api.messages.getDialogs(count=200, offset=count)
            for i in range(1, len(dialogs)):
                if 'attachments' in dialogs[i].keys():
                    if dialogs[i]['attachments'][0]['type'] == 'photo':
                        if not self.checkIfAnwsered(dialogs[i]['uid']):
                            if dialogs[i]['body'].lower() == 'светлый':
                                photo = dialogs[i]['attachments'][0]['photo']
                                self.addToDownloadQueue(photo['src_big'], 'light', dialogs[i]['uid'])
                            elif dialogs[i]['body'].lower() == 'синий':
                                photo = dialogs[i]['attachments'][0]['photo']
                                self.addToDownloadQueue(photo['src_big'], 'blue', dialogs[i]['uid'])
                            elif dialogs[i]['body'].lower() == 'чб':
                                photo = dialogs[i]['attachments'][0]['photo']
                                self.addToDownloadQueue(photo['src_big'], 'monochrome', dialogs[i]['uid'])
            count += 200



if __name__ == '__main__':
    download_worker.start_workers(config.download_tokens)
    download_worker.control_thread()

    ImageWorker.start_workers(config.send_tokens)
    ImageWorker.control_thread()

    bot = Bot(config.tokens[0])
    bot.botLogic()


    while ImageWorker.queue != Empty:
        print('Working')
        time.sleep(5)

