#!/usr/bin/python3
from PIL import Image, ImageEnhance


def render_blue(img_name):
    try:
        img = Image.open('images/' + img_name)
    except Exception as e:
        print('Img load exception{0}'.format(e))
        return None
    width, height = img.size
    if width >= 2560 or height >= 2560:
        print('Img size exception')
        return None
    try:
        pencil_img = Image.open('filter.jpg')
        pencil_img = pencil_img.resize((width, height), Image.ANTIALIAS)
    except Exception as e:
        print("cannot create thumbnail for '%s'" % 'filter.jpg')
        return None
    img = ImageEnhance.Contrast(img).enhance(2)
    img_pixels = list(img.getdata())
    pencil_pixels = list(pencil_img.getdata())

    try:
        for i in range(len(img_pixels)):
            r, g, b = img_pixels[i]
            pr, pb, pg = pencil_pixels[i]
            pr /= 255  # (255 - pr + 1)
            pb /= 150  # (255 - pb + 1)
            pg /= 255  # (255 - pg + 1)
            r /= 255
            b /= 150
            g /= 255
            img_pixels[i] = (int(150 * pr * r), int(150 * pb * g), int(255 * pg * b))
    except Exception as e:
        print('Img render exception{0}'.format(e))
    image_out = Image.new(img.mode, (width, height))
    image_out.putdata(img_pixels)
    image_out.save('images/'+img_name+'_render_blue.jpg')
    return 'images/'+img_name+'_render_blue.jpg'


def render_norm(img_name):
    try:
        img = Image.open('images/' + img_name)
    except Exception as e:
        print('Img load exception{0}'.format(e))
        return None
    width, height = img.size
    if width > 2000 or height > 2000:
        print('Img size exception')
        return None
    try:
        pencil_img = Image.open('filter.jpg')
        pencil_img = pencil_img.resize((width, height), Image.ANTIALIAS)
    except Exception as e:
        print("cannot create thumbnail for '%s'" % 'filter.jpg')
        return None
    img = ImageEnhance.Contrast(img).enhance(2)
    img_pixels = list(img.getdata())
    pencil_pixels = list(pencil_img.getdata())

    try:
        for i in range(len(img_pixels)):
            r, g, b = img_pixels[i]
            pr, pb, pg = pencil_pixels[i]
            pr /= 200  # (255 - pr + 1)
            pb /= 200  # (255 - pb + 1)
            pg /= 200  # (255 - pg + 1)
            r /= 200
            b /= 200
            g /= 200
            img_pixels[i] = (int(200 * pr * r), int(200 * pb * g), int(200 * pg * b))
    except Exception as e:
        print('Img render exception{0}'.format(e))
    image_out = Image.new(img.mode, (width, height))
    image_out.putdata(img_pixels)
    image_out.save('images/'+img_name+'_render_norm.jpg')
    return 'images/'+img_name+'_render_norm.jpg'


def render_monochrome(img_name):
    try:
        img = Image.open('images/' + img_name).convert('LA').convert('RGB')
    except Exception as e:
        print('Img load exception{0}'.format(e))
        return None
    width, height = img.size
    if width > 2000 or height > 2000:
        print('Img size exception')
        return None
    try:
        pencil_img = Image.open('filter.jpg')
        pencil_img = pencil_img.resize((width, height), Image.ANTIALIAS)
    except Exception as e:
        print("cannot create thumbnail for '%s'" % 'filter.jpg')
        return None
    img = ImageEnhance.Contrast(img).enhance(2)
    img_pixels = list(img.getdata())
    pencil_pixels = list(pencil_img.getdata())

    try:
        for i in range(len(img_pixels)):
            r, g, b = img_pixels[i]
            pr, pb, pg = pencil_pixels[i]
            pr /= 200  # (255 - pr + 1)
            pb /= 200  # (255 - pb + 1)
            pg /= 200  # (255 - pg + 1)
            r /= 200
            b /= 200
            g /= 200
            img_pixels[i] = (int(200 * pr * r), int(200 * pb * g), int(200 * pg * b))
    except Exception as e:
        print('Img render exception{0}'.format(e))
    image_out = Image.new(img.mode, (width, height))
    image_out.putdata(img_pixels)
    image_out.save('images/'+img_name+'_render_monochrome.jpg')
    return 'images/'+img_name+'_render_monochrome.jpg'
